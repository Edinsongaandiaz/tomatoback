from django.db import models
import datetime
from django.contrib.auth.models import User
import nexmo
from django.conf import settings
from django.db.models.signals import post_save

ESTADOCULTIVO_CHOICES = (
    ("1", "Aislamiento"),
    ("2", "Siembra y cultivos"),
    ("3", "Cosecha"),
    ("4", "Finalizado"),
)
class TipoPago(models.Model):
    nombre = models.CharField(max_length=40)
    valor = models.IntegerField()

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Tipo De Pago"
        verbose_name_plural = "Tipos De Pagos"


class TipoCultivo(models.Model):
    nombre = models.CharField(max_length=40)
    dias = models.IntegerField()
    valor_kilo = models.IntegerField()

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Tipo De Cultivo"
        verbose_name_plural = "Tipos De Cultivos"


class TipoCantidad(models.Model):
    nombre = models.CharField(max_length=40)

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Tipo Cantidad"
        verbose_name_plural = "Tipos De Cantidades"


class Cultivo(models.Model):
    nombre = models.CharField(max_length=40)
    descripcion = models.TextField()
    fecha_inicial = models.DateField()
    proceso_actual = models.CharField(
        max_length=2,
        choices=ESTADOCULTIVO_CHOICES,
        default='1'
    )
    fecha_final = models.DateField(auto_now_add=False)
    activo = models.BooleanField(verbose_name="¿Ya se sembro el cultivo?")
    tipo_cultivo = models.ForeignKey(
        TipoCultivo, null=False, blank=False, on_delete=models.CASCADE)
    administrador = models.ForeignKey(
        User, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Cultivo"
        verbose_name_plural = "Cultivos"

    @property
    def calcular_fecha_fin(self, *args, **kwargs):
        return (self.fecha_inicial + datetime.timedelta(days=self.tipo_cultivo.dias))

    def save(self, *args, **kwargs):
        self.fecha_final = self.calcular_fecha_fin
        super(Cultivo, self).save()


class Pagos(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tipo_pago = models.ForeignKey(TipoPago, on_delete=models.CASCADE)
    cantidad = models.FloatField()
    total_pago = models.FloatField(blank=True, null=True)
    fecha = models.DateTimeField(auto_now=True)
    cultivo = models.ForeignKey(
        Cultivo, null=False, blank=False, on_delete=models.CASCADE)

    @property
    def calcular_pago(self, *args, **kwargs):
        return (self.tipo_pago.valor * self.cantidad)

    def save(self, *args, **kwargs):
        self.total_pago = self.calcular_pago
        super(Pagos, self).save()

    class Meta:
        verbose_name = "Pago"
        verbose_name_plural = "Pagos"


class Producto(models.Model):
    nombre = models.CharField(max_length=40)
    minimo = models.IntegerField()
    actual = models.IntegerField()
    tipo_cantidad = models.ForeignKey(
        TipoCantidad, null=False, blank=False, on_delete=models.CASCADE)
    valor_unidad = models.IntegerField()

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Producto"
        verbose_name_plural = "Productos"

    @property
    def calcular_fecha_fin(self, *args, **kwargs):
        return (self.actual <= self.minimo)

    def save(self, *args, **kwargs):
        cliente = nexmo.Client(
            key='76a4e5e9', secret='RQG674AHW6sncHw2')
        cliente.send_message({
            'from':  'API de SMS de Vonage',
            'to':  '573207289628',
            'text':  "El inventario de {0} esta bajo, hay {1} de {2} {3}".format(self.nombre, self.minimo, self.actual, self.tipo_cantidad),
        })
        super(Producto, self).save()


class SolicitudProducto(models.Model):
    Producto = models.ForeignKey(
        Producto, null=False, blank=False, on_delete=models.CASCADE)
    Cultivo = models.ForeignKey(
        Cultivo, null=False, blank=False, on_delete=models.CASCADE)
    cantidad = models.IntegerField()
    fecha_solicitud = models.DateTimeField(auto_now_add=True, editable=False)
    valor = models.IntegerField(editable=False)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE)

    def __str__(self):
        return "{0}".format(self.Producto)

    class Meta:
        verbose_name = "Solicitud de Producto"
        verbose_name_plural = "Solicitud de Productos"

    @property
    def calcular_cantidad(self, *args, **kwargs):
        return ((self.actual - self.minimo) < 0)

    def save(self, *args, **kwargs):
        self.valor = self.Producto.valor_unidad * self.cantidad
        super(SolicitudProducto, self).save()


class VentasCultivo(models.Model):
    nombre = models.CharField(max_length=40)
    fecha_venta = models.DateTimeField(auto_now_add=True, editable=False)
    Cultivo = models.ForeignKey(
        Cultivo, null=False, blank=False, on_delete=models.CASCADE)
    cantidad_vendida = models.PositiveIntegerField()
    valor = models.FloatField(editable=False)

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Venta Cultivo"
        verbose_name_plural = "Ventas Cultivos"

    @property
    def calcular_venta(self, *args, **kwargs):
        valor = 0
        todas_ventas = Cultivo.objects.all()
        for ventas in todas_ventas:
            if (ventas == self.Cultivo):
                valor = ventas.tipo_cultivo.valor_kilo * self.cantidad_vendida
        return valor

    def save(self, *args, **kwargs):
        self.valor = self.calcular_venta
        super(VentasCultivo, self).save()


class GastoCultivo(models.Model):
    nombre = models.CharField(max_length=40)
    fecha_gasto = models.DateTimeField(auto_now_add=True, editable=False)
    Cultivo = models.ForeignKey(
        Cultivo, null=False, blank=False, on_delete=models.CASCADE)
    valor = models.FloatField()

    def __str__(self):
        return "{0}".format(self.nombre)

    class Meta:
        verbose_name = "Gasto Cultivo"
        verbose_name_plural = "Gastos Cultivos"


class Totalizados(models.Model):
    Cultivo = models.OneToOneField(
        Cultivo, null=False, blank=False, on_delete=models.CASCADE, primary_key=True)
    ventas = models.FloatField(editable=False)
    gastos = models.FloatField(editable=False)
    estado = models.FloatField(editable=False)

    @property
    def calcular_ventas(self, *args, **kwargs):
        valor = 0
        todas_ventas = VentasCultivo.objects.all()
        for ventas in todas_ventas:
            if (ventas.Cultivo == self.Cultivo):
                valor += ventas.valor
        return valor

    class Meta:
        verbose_name = "Totalizador Cultivo"
        verbose_name_plural = "Totalizador Cultivos"

    @property
    def calcular_gastos(self, *args, **kwargs):
        valor = 0
        todas_gastos = GastoCultivo.objects.all()
        todas_gasto = SolicitudProducto.objects.all()
        todas_gast = Pagos.objects.all()
        for gasto in todas_gast:
            if (gasto.cultivo == self.Cultivo):
                valor += gasto.total_pago
        for gasto in todas_gasto:
            if (gasto.Cultivo == self.Cultivo):
                valor += gasto.valor
        for gastos in todas_gastos:
            if (gastos.Cultivo == self.Cultivo):
                valor += gastos.valor
        return -valor

    def save(self, *args, **kwargs):
        self.ventas = self.calcular_ventas
        self.gastos = self.calcular_gastos
        self.estado = self.ventas + self.gastos
        super(Totalizados, self).save()
