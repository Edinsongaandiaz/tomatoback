from django.test import TestCase
from app.models import Cultivo,TipoCultivo,Producto,TipoCantidad,TipoPago,Pagos
from django.contrib.auth.models import User
from django.utils import timezone
import datetime

class Test(TestCase):
    def setUp(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        TipoCultivo.objects.create(nombre="papas",dias=222,valor_kilo=1000)
        t = TipoCultivo.objects.get(id=1)
        c =Cultivo.objects.create(nombre="cultivo de papa", descripcion="null",tipo_cultivo=t,fecha_inicial=datetime.datetime(2018, 6, 1),
          fecha_final=datetime.datetime(2018, 6, 1).date(),activo=True,administrador=user,proceso_actual=1)
        tc=TipoCantidad.objects.create(nombre="unidades")
        producto = Producto.objects.create(nombre="papas",minimo=100,actual=200,tipo_cantidad=tc,valor_unidad=5000)
        tp=TipoPago.objects.create(nombre="cheque",valor=20000)
        pago=Pagos.objects.create(user=user,tipo_pago=tp,cantidad=20.0,fecha=timezone.now(),cultivo=c)

    def test1(self):
        c = Cultivo.objects.get(nombre="cultivo de papa")
        self.assertEqual(c.descripcion, 'null')
    def test2(self):
        t = TipoCultivo.objects.get(id=1)
        self.assertEqual(t.nombre, 'papas')
    def test3(self):
        t = TipoCultivo.objects.get(id=1)
        self.assertEqual(t.dias,222)
    def test4(self):
        t = TipoCultivo.objects.get(id=1)
        c = Cultivo.objects.get(nombre="cultivo de papa")
        self.assertEqual(c.tipo_cultivo,t) 
    def test5(self):
        c = Cultivo.objects.get(nombre="cultivo de papa")
        self.assertEqual(c.fecha_inicial,datetime.datetime(2018, 6, 1).date()) 
    def test6(self):
        user = User.objects.get(username='john')
        c = Cultivo.objects.get(nombre="cultivo de papa")
        self.assertEqual(c.administrador,user) 
    def test7(self):
        c = Cultivo.objects.get(nombre="cultivo de papa")
        self.assertEqual(c.activo, True) 
    def test8(self):
        tc=TipoCantidad.objects.get(nombre="unidades")
        producto = Producto.objects.get(nombre="papas")
        self.assertEqual(producto.tipo_cantidad, tc)    
    def test9(self):
        tp=TipoPago.objects.get(nombre="cheque")
        pago=Pagos.objects.get(id=1)
        self.assertEqual(pago.tipo_pago, tp) 
    def test10(self):
        tp=TipoPago.objects.get(nombre="cheque")
        pago=Pagos.objects.get(id=1)
        self.assertEqual(pago.tipo_pago.valor, 20000) 
    def test11(self):
        user = User.objects.get(username='john')
        pago=Pagos.objects.get(id=1)
        self.assertEqual(pago.user, user)
    def test12(self):
        tp=TipoPago.objects.get(nombre="cheque")
        pago=Pagos.objects.get(id=1)
        self.assertEqual(pago.total_pago, tp.valor*pago.cantidad) 
         