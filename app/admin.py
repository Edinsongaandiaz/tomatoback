from django.contrib import admin
from .models import Pagos, TipoCantidad, TipoPago, Cultivo, Producto, TipoCultivo, Totalizados, VentasCultivo, GastoCultivo, SolicitudProducto


class pagosAdmin(admin.ModelAdmin):

    list_filter = (
        'user',
        'fecha',
    )

    list_display = (
        'user',
        'fecha',
        'tipo_pago',
        'cantidad',
        'total_pago',
    )

    search_fields = ('user', )


admin.site.register(Pagos, pagosAdmin)


class tipoCantidadAdmin(admin.ModelAdmin):

    list_filter = ('nombre', )

    list_display = ('nombre', )

    search_fields = ('nombre', )


admin.site.register(TipoCantidad, tipoCantidadAdmin)


class tipoPagoAdmin(admin.ModelAdmin):

    list_filter = ('nombre', )

    list_display = (
        'nombre',
        'valor',
    )

    search_fields = ('nombre', )


admin.site.register(TipoPago, tipoPagoAdmin)


class cultivoAdmin(admin.ModelAdmin):

    list_filter = (
        'nombre',
        'activo',
    )

    list_display = (
        'nombre',
        'tipo_cultivo',
        'fecha_inicial',
        'activo',
        'fecha_final',
        'administrador',
    )

    search_fields = (
        'nombre',
        'administrador',
    )


admin.site.register(Cultivo, cultivoAdmin)


class tipoCultivoAdmin(admin.ModelAdmin):

    list_display = (
        'nombre',
        'dias',
        'valor_kilo',
    )

    search_fields = ('nombre', )


admin.site.register(TipoCultivo, tipoCultivoAdmin)


class productoAdmin(admin.ModelAdmin):

    list_filter = ('tipo_cantidad', )

    list_display = (
        'nombre',
        'actual',
        'minimo',
        'tipo_cantidad',
    )

    search_fields = ('nombre', )


admin.site.register(Producto, productoAdmin)


class totalizadosAdmin(admin.ModelAdmin):

    list_display = (
        'Cultivo',
        'ventas',
        'gastos',
        'estado',
    )

    search_fields = ('Cultivo', )


admin.site.register(Totalizados, totalizadosAdmin)


class ventasCultivoAdmin(admin.ModelAdmin):

    list_filter = ('Cultivo', )

    list_display = (
        'nombre',
        'fecha_venta',
        'Cultivo',
        'valor',
    )

    search_fields = ('nombre', )


admin.site.register(VentasCultivo, ventasCultivoAdmin)


class gastoCultivoAdmin(admin.ModelAdmin):

    list_filter = ('Cultivo', )

    list_display = (
        'nombre',
        'fecha_gasto',
        'Cultivo',
        'valor',
    )

    search_fields = ('nombre', )


admin.site.register(GastoCultivo, gastoCultivoAdmin)


class solicitudProductoAdmin(admin.ModelAdmin):

    list_filter = ('Cultivo', )

    list_display = (
        'Producto',
        'fecha_solicitud',
        'Cultivo',
        'valor',
    )

    search_fields = ('Producto', )


admin.site.register(SolicitudProducto, solicitudProductoAdmin)
